<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('isikud', function (Blueprint $table) {
            $table->increments('id');
            $table->string('eesnimi');
            $table->string('perenimi');
            $table->string('isikukood');
            $table->string('maakond');
            $table->string('linn');
            $table->string('streetname');
            $table->string('streetnumber');
            $table->integer('appartment');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('isikud');
    }
}
