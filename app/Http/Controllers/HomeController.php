<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = \DB::table('isikud')
            ->where('isikukood', Auth::user()->isikukood)
            ->get();

        $resultArray = json_decode(json_encode($data), true);

        return view('home', ['data' => $resultArray]);
    }
}
