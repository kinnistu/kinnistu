<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class ApiController extends Controller
{
    public function index()
    {
        return view('api');
    }

    public function request(Request $request) {
        return json_encode(DB::table('isikud')->where('isikukood', '=', $request->input('personal_code'))->get()->toArray());
    }
}
