<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class SearchController extends Controller
{
    public function index()
    {
        return view('search');
    }

    public function submit(Request $request)
    {

        $d_data = \DB::table('isikud')
            ->where('isikukood', $request->input('ik'))
            ->get();

        $d_data = json_decode($d_data, true);

        if (empty($d_data)) {
            return view('search', ['sub' => 'yes']);
        } else {

            $url = 'https://projects.diarainfra.com/xtee/request/';

            $data = [
                'socialId1' => $request->input('ik'),
                'socialId2' => $request->input('ik'),
                'organization' => 'Kinnisturegister',
                'destination' => 'Rahvastikuregister',
                'request' => 'Kinnistu-request',
                'endpoint' => 'persons/get?person_code=' . $request->input('ik')
            ];

            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

            $json = curl_exec($ch);
            curl_close($ch);

            $p_data = json_decode($json, true);

            if ($p_data['status'] == 400) {
                $p_data = array('data' => array(
                    'person_first_name' => '',
                    'person_last_name' => ''
                ));
            }

            if (!isset($p_data['data'])) {
                $p_data = array('data' => array(
                    'person_first_name' => '',
                    'person_last_name' => ''
                ));
            }

            $data_out = array();

            foreach ($d_data as $data) {
                array_push($data_out, array_merge($data, $p_data['data']));
            }

            $data_out = array('data' => $data_out);
            //return view('search', var_dump($data_out));
            return view('search', ['sub' => 'yes', 'data' => $data_out]);
        }
    }
}
