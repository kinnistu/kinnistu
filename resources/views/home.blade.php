@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                        <?php
                        if(empty($data)) {
                            if(isset($sub)) {
                                echo '<div class="col-md-8">Otsing ei andnud ühtegi tulemust!</div>';
                            } else {
                                echo '<div class="col-md-8">Esmalt täitke soovitud lahtrid.</div>';
                            }
                        } else { ?>
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Eesnimi</th>
                                <th>Perekonna nimi</th>
                                <th>Isikukood</th>
                                <th>Kinnistu nimi</th>
                                <th>Aadress</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            foreach($data as $result) {
                                echo '<tr>';
                                echo '<td>'.$result['eesnimi'].'</td>';
                                echo '<td>'.$result['perenimi'].'</td>';
                                echo '<td>'.$result['isikukood'].'</td>';
                                echo '<td></td>';
                                echo '<td></td>';
                                echo '</tr>';
                            }
                            }
                            ?>
                            </tbody>
                        </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
