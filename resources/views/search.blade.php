@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <form action="{{url('/search/submit')}}" method="post" role="form">
                            <legend>Otsing</legend>

                            <div class="form-group col-md-12">
                                <label>Isikukood</label>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="text" class="form-control" name="ik" id="ik" placeholder="12345678910">
                            </div>

                            <div class="form-group col-md-12">
                                <button type="submit" class="btn btn-primary">Otsi</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Otsingu tulemus</h3>
                    </div>
                    <div class="panel-body">
                                <?php
                                    if(empty($data)) {
                                        if(isset($sub)) {
                                            echo '<div class="col-md-8">Otsing ei andnud ühtegi tulemust!</div>';
                                        } else {
                                            echo '<div class="col-md-8">Esmalt täitke soovitud lahtrid.</div>';
                                        }
                                    } else { ?>
                                    <table class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th>Eesnimi</th>
                                            <th>Perekonna nimi</th>
                                            <th>Isikukood</th>
                                            <th>Aadress</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        foreach($data['data'] as $result) {
                                            echo '<tr>';
                                            echo '<td>'.(!empty($result['person_first_name']) ? $result['person_first_name'] : 'Ei tuvastatud').'</td>';
                                            echo '<td>'.(!empty($result['person_last_name']) ? $result['person_last_name'] : 'Ei tuvastatud').'</td>';
                                            echo '<td>'.(!empty($result['isikukood']) ? $result['isikukood'] : 'Ei tuvastatud').'</td>';
                                            echo '<td>'.$result['maakond'].', '.$result['linn'].', '.$result['streetname'].', '.$result['streetnumber'].', '.$result['appartment'].'</td>';
                                            echo '</tr>';
                                        }
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
