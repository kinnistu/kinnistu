-- MySQL dump 10.16  Distrib 10.1.26-MariaDB, for Win32 (AMD64)
--
-- Host: localhost    Database: kinnistu
-- ------------------------------------------------------
-- Server version	10.1.26-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `isikud`
--

DROP TABLE IF EXISTS `isikud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `isikud` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `eesnimi` varchar(55) NOT NULL,
  `perenimi` varchar(55) NOT NULL,
  `isikukood` varchar(11) NOT NULL,
  `maakond` varchar(55) NOT NULL,
  `linn` varchar(55) DEFAULT NULL,
  `streetname` varchar(55) NOT NULL,
  `streetnumber` varchar(5) DEFAULT NULL,
  `appartment` int(3) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `isikud`
--

LOCK TABLES `isikud` WRITE;
/*!40000 ALTER TABLE `isikud` DISABLE KEYS */;
INSERT INTO `isikud` VALUES (1,'Aigar','Sander','23455312324','Tartumaa','Tartu','Raja','42',1),(2,'Rauno','Varul','39393939393','Tartumaa','Tartu','Teguri','45D',4),(3,'Evert','Orti','30303030303','Tartumaa','Tartu','Kesk','51',NULL);
/*!40000 ALTER TABLE `isikud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `eesnimi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `perenimi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isikukood` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'demo','demo@example.com','','','$2y$10$em5WM5Nvt2a8CCBd1iqBqO/w7j9QHvaeA4uhb46b45vJbjkkgQ4Q2','FqAJlI1TjEEpGWKSTdYrGLHeNayZZWb3k7wJm6Ax8aE4FOA46VB4taJJ432c','2017-09-25 06:37:15','2017-09-25 06:37:15'),(2,NULL,NULL,NULL,'test@testmail.test','$2y$10$Gv6m.2ea8oHCqbrcfaiNDOlSQ460lagGOBIQ2CEz.Gbhq5EHV8QVq','ochJYL6io8qeUkbkiRkwqm8WN6RhmBC896XcohVTX4zzdy9RARZrTuNNUy56','2017-09-25 10:10:05','2017-09-25 10:10:05'),(3,NULL,NULL,NULL,'test1@testmail.test','$2y$10$UC7MRTRaTvefkSxuxYGFGe4K3p/ajJu49QNVKumdS3wYxOazd84vW','0S2Xo9Y0m61HF9DJcCPxrt0rcyjjT2hOHAVh1nZALtStgDZqTmcviwsupKMT','2017-09-25 10:11:56','2017-09-25 10:11:56'),(4,NULL,NULL,NULL,'test2@testmail.test','$2y$10$UF.nksu1p3KLMy3PLQYeeexjyiTDY/Q1lIGm1kGu./QJucrNKnjG6','TNZxASsDdXQvT3lJkdFWDHcQv6rNyE6LsZKvvc8SHvunlXB46nh4k5n3zSUJ','2017-09-25 10:16:04','2017-09-25 10:16:04'),(5,'','','','test3@testmail.test','$2y$10$KAX3hJgpfadbQ1t36oy3PueMkGTgXKYsz/.RBUg2uBP2IVCusxE0K','0IfiwLd3I4Zah4VdngEbIfTJYgqy0dAVhOxNGS9u0EKz1Liwwe3o62ENSjn1','2017-09-25 10:19:42','2017-09-25 10:19:42'),(6,'','','','test4@testmail.test','$2y$10$NiuuSilHbApDqmEhhYpz0.rRighg9e/UJybkQ8LjojF6XcRPIcR/.','tJvm9J9FwG4JeoCTiQw7k3rtMQAwkDf9thA4XBvKoROIPI3XazfdHYj7pLg9','2017-09-25 10:23:17','2017-09-25 10:23:17'),(7,NULL,NULL,NULL,'test5@testmail.test','$2y$10$LNdYhtVnLxuHR.9iECd0puSmN/x2orfF2VuMA8h89T75JoXxBSS0i','WWdXqdOGfVLKrthJrImurifsrYS6Lemc16JH78UnGsEHoj1Kwz1SfBvxhw7D','2017-09-25 10:24:16','2017-09-25 10:24:16'),(8,NULL,NULL,NULL,'test6@testmail.test','$2y$10$uXmnNSXtHr0r3NNc3xPPxOqHW1rutFENSu42cZYJ8iuXOaLk.MOjO','pupVnRVnQOiuve2tsUbiEr3F1WC8f3R9hZbu6rdcdrgztvW816dIzW3d0bX4','2017-09-25 10:31:56','2017-09-25 10:31:56'),(9,'rr','ee','23455312324','test7@testmail.test','$2y$10$hn60FC/5qDcvYvdlcX8gGOGc2Hdb0KcLIAFi6w.YFZ8aaVvVsqvA6','g1QeENlf66GlNpSGqkba5tlv6oXfuoGaUkfvh8CaxyxjDhX4cJ3T3pBpkYKH','2017-09-25 10:33:23','2017-09-25 10:33:23'),(11,'Aigar','Sander','39610192726','aigar.sander@khk.ee','$2y$10$WAcP/qphjVJP2QIyDu1pkOmLZZmJgVnS/CRo32.1DIMbI7F9d0w6W','4wFZ286QsxHHbDzrhAjR7vv6GPUzUWtk6NgWKhdbu9av7nd0da2BlWsVdRvK','2017-10-09 07:09:12','2017-10-09 07:09:12');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-10-09 13:18:54
